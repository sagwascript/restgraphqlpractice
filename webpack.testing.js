const nodeExternals = require('webpack-node-externals')
const path = require('path')
process.env.NODE_ENV = 'testing'

module.exports = {
  target: 'node',
  mode: 'development',
  output: {
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
  },
  resolve: {
    alias: {
      '~/testhelpers': path.resolve(__dirname, 'test/helpers'),
      '~testhelpers': path.resolve(__dirname, 'test/helpers'),
      '~apiSpec': path.resolve(__dirname, 'test/apiSpec'),
      '~/apiSpec': path.resolve(__dirname, 'test/apiSpec'),
      '~/config': path.resolve(__dirname, 'src/config/index')
    }
  },
  devtool: 'cheap-module-source-map',
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: ['transform-regenerator', 'transform-runtime']
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(graphql|gql)/,
        loader: 'raw-loader',
        exclude: /(node_modules)/
      }
    ]
  }
}
