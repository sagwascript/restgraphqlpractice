import app from './server'

import config from './config'

const PORT = config.port

app.listen(PORT, () => {
  console.log(`I'm listening on port ${PORT}`)
})
