import express from 'express'

import { restRouter, graphQLRouter } from './api'
import setupMidware from './middleware'
import { connect } from './db'
import { protect, signin } from './api/modules/auth'
import { graphiqlExpress } from 'apollo-server-express'

const app = express()

// connect to the db
connect()
// setup middleware for global app
setupMidware(app)

// mount signin middleware to /signin route
app.use('/signin', signin)

// mount restRouter route to /api
app.use('/api', protect, restRouter)

app.use('/graphql', protect, graphQLRouter)
app.use('/docs', graphiqlExpress({ endpointURL: '/graphql' }))

app.all('*', (req, res) => {
  res.sendStatus(404).json({ message: 'Not found!' })
})

export default app
