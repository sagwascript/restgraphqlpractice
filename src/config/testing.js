export const config = {
  expireTime: '30d',
  secrets: {
    JWT_SECRET: 'pr3ttyc00lth0'
  },
  db: {
    url: 'mongodb://localhost/jams-test'
  },
  disableAuth: false
}
