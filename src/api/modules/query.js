import merge from 'lodash.merge'

const controller = {
  findByParam(model, id) {
    return model.findById(id).exec()
  },

  createOne(model, body) {
    return model.create(body)
  },

  getAll(model) {
    return model.find({}).exec()
  },

  getOne(docToGet) {
    return Promise.resolve(docToGet)
  },

  updateOne(docToUpdate, update) {
    merge(docToUpdate, update)
    return docToUpdate.save()
  },

  deleteOne(docToDelete) {
    return docToDelete.remove()
  }
}

export const findByParam = model => (req, res, next, id) => {
  return controller
    .findByParam(model, id)
    .then(doc => {
      if (!doc) {
        next(new Error('Not Found!'))
      } else {
        req.docFromId = doc
        next()
      }
    })
    .catch(err => next(err))
}

export const getAll = model => (req, res, next) => {
  return controller
    .getAll(model)
    .then(docs => res.json(docs))
    .catch(err => next(err))
}

export const getOne = () => (req, res, next) => {
  return controller
    .getOne(req.docFromId)
    .then(doc => res.status(201).json(doc))
    .catch(err => next(err))
}

export const createOne = model => (req, res, next) => {
  return controller
    .createOne(model, req.body)
    .then(doc => res.status(201).json(doc))
    .catch(err => next(err))
}

export const updateOne = () => async (req, res, next) => {
  const docToUpdate = req.docFromId
  const update = req.body

  return controller
    .updateOne(docToUpdate, update)
    .then(doc => res.status(201).json(doc))
    .catch(err => next(err))
}

export const deleteOne = () => (req, res, next) => {
  return controller
    .deleteOne(req.docFromId)
    .then(doc => res.status(201).json(doc))
    .catch(err => next(err))
}

export const generateController = (model, overrides = {}) => {
  const defaults = {
    getAll: getAll(model),
    getOne: getOne(),
    createOne: createOne(model),
    updateOne: updateOne(),
    deleteOne: deleteOne(),
    findByParam: findByParam(model)
  }

  return { ...defaults, ...overrides }
}
