import express from 'express'
import { userRouter } from './resources/user'
import { playlistRouter } from './resources/playlist'
import { songRouter } from './resources/song'
import { errorHandler } from './modules/errorHandler'

export const restRouter = express.Router()

restRouter.use('/user', userRouter)
restRouter.use('/song', songRouter)
restRouter.use('/playlist', playlistRouter)
restRouter.use(errorHandler)
