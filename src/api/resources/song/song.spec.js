// import createApiSpec from '~/apiSpec'
import { describe, it, beforeEach, afterEach } from 'mocha'
import { expect } from 'chai'
import { Song } from './song.model'
import { dropDb, runQuery, createDummyUser } from '~/testhelpers'

// createApiSpec(Song, 'song', { title: 'downtown jamming', url: 'http://music.mp3' })

describe('Query/Mutation graphQL on Song', () => {
  let user

  beforeEach(async () => {
    await dropDb()
    user = await createDummyUser()
  })

  afterEach(async () => {
    await dropDb()
  })

  it('should get all the songs', async () => {
    const res = await runQuery(
      `
      {
        allSongs {
          id
          title
        }
      }
    `,
      {},
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.allSongs).to.be.an('array')
  })

  it('should get one song', async () => {
    const song = await Song.create({ title: 'that song', url: 'www.music.mp3' })
    const res = await runQuery(
      `
      {
        Song(id: "${song.id}") {
          id
          title
        }
      }
    `,
      {},
      user
    )

    expect(res.error).to.not.exist
    expect(res.data.Song).to.be.an('object')
    expect(res.data.Song.id).to.be.eql(song.id.toString())
  })

  it('should update a song', async () => {
    const song = await Song.create({ title: 'that song', url: 'www.music.mp3' })
    const newSong = { id: song.id, title: 'this song' }
    const res = await runQuery(
      `
      mutation update($input: UpdatedSong!) {
        updateSong(input: $input) {
          id
          title
        }
      }
    `,
      { input: newSong },
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.updateSong).to.be.an('object')
    expect(res.data.updateSong.id).to.be.eql(newSong.id)
  })

  it('should create a song', async () => {
    const newSong = { title: 'that song', url: 'www.that.mp3' }
    const res = await runQuery(
      `
      mutation create($input: NewSong!) {
        createSong(input: $input) {
          id
          title
        }
      }
    `,
      { input: newSong },
      user
    )
    expect(res.errors).to.not.exist
    expect(res.data.createSong).to.be.an('object')
  })

  it('should delete a song', async () => {
    const newSong = await Song.create({ title: 'that song', url: 'www.that.mp3' })
    const res = await runQuery(
      `
      mutation delete($id: ID!) {
        deleteSong(id: $id) {
          id
          title
        }
      }
    `,
      { id: newSong.id },
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.deleteSong).to.be.an('object')
    expect(res.data.deleteSong.id).to.be.eql(newSong.id)
  })
})
