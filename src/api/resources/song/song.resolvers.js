import { Song } from './song.model'

const allSongs = () => {
  return Song.find({}).exec()
}

const getOne = (_, { id }) => {
  return Song.findById(id).exec()
}

const createSong = (_, { input }) => {
  return Song.create(input)
}

const updateSong = (_, { input }) => {
  const { id, ...update } = input
  return Song.findByIdAndUpdate(id, update, { new: true })
}

const deleteSong = (_, { id }) => {
  return Song.findByIdAndRemove(id)
}

export const songResolvers = {
  Query: {
    allSongs,
    Song: getOne
  },
  Mutation: {
    createSong,
    updateSong,
    deleteSong
  }
}
