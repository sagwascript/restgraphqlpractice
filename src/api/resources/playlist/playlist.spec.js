// import createApiSpec from '~/apiSpec'
import { expect } from 'chai'
import { describe, it, beforeEach, afterEach } from 'mocha'
import { dropDb, runQuery, createDummyUser } from '~/testhelpers'

import { Playlist } from './playlist.model'
// createApiSpec(Playlist, 'playlist', { title: 'my playlist' })

describe('Query/Mutation graphQL on Playlist', async () => {
  let user

  beforeEach(async () => {
    await dropDb()
    user = await createDummyUser()
  })

  afterEach(async () => {
    await dropDb()
  })

  it('should get all of the playlists', async () => {
    const res = await runQuery(
      `
      {
        allPlaylists {
          id
          title
        }
      }
    `,
      {},
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.allPlaylists).to.be.an('array')
  })

  it('should get one playlist', async () => {
    const newPlaylist = await Playlist.create({ title: 'my fav', songs: [] })
    const res = await runQuery(
      `
      {
        Playlist(id: "${newPlaylist.id}") {
          id
          title
        }
      }
    `,
      {},
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.Playlist).to.be.an('object')
    expect(res.data.Playlist.id).to.be.eql(newPlaylist.id.toString())
  })

  it('should create a playlist', async () => {
    const newPlaylist = { title: 'my fav', songs: [], favorite: false }
    const res = await runQuery(
      `
      mutation create($input: NewPlaylist!) {
        createPlaylist(input: $input) {
          id
          title
        }
      }
    `,
      { input: newPlaylist },
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.createPlaylist).to.be.an('object')
    expect(res.data.createPlaylist.title).to.be.eql(newPlaylist.title.toString())
  })

  it('should update a playlist', async () => {
    const playlist = await Playlist.create({ title: 'my fav', songs: [], favorite: false })
    const res = await runQuery(
      `
      mutation update($input: UpdatedPlaylist!) {
        updatePlaylist(input: $input) {
          id
          title
        }
      }
    `,
      { input: { id: playlist.id, title: 'another fav', songs: [] } },
      user
    )

    expect(res.error).to.not.exist
    expect(res.data.updatePlaylist).to.be.an('object')
    expect(res.data.updatePlaylist.title).to.be.eql('another fav')
  })

  it('should delete a playlist', async () => {
    const playlist = await Playlist.create({ title: 'my fav', songs: [], favorite: false })
    const res = await runQuery(
      `
      mutation delete($id: ID!) {
        deletePlaylist(id: $id) {
          id
          title
        }
      }
    `,
      { id: playlist.id },
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.deletePlaylist).to.be.an('object')
    expect(res.data.deletePlaylist.id).to.be.eql(playlist.id.toString())
  })
})
