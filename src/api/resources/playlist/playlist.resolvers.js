import { Playlist } from './playlist.model'

const allPlaylists = () => {
  return Playlist.find({}).exec()
}

const getOne = (_, { id }) => {
  return Playlist.findById(id).exec()
}

const createPlaylist = (_, { input }) => {
  return Playlist.create(input)
}

const updatePlaylist = (_, { input }) => {
  const { id, ...update } = input
  return Playlist.findByIdAndUpdate(id, update, { new: true })
}

const deletePlaylist = (_, { id }) => {
  return Playlist.findByIdAndRemove(id)
}

const populateSong = async playlist => {
  const res = await playlist.populate('songs').execPopulate()
  return res.songs
}

export const playlistResolvers = {
  Query: {
    allPlaylists,
    Playlist: getOne
  },
  Mutation: {
    createPlaylist,
    updatePlaylist,
    deletePlaylist
  },
  Playlist: {
    songs: populateSong
  }
}
